module Model.Variables where

open import Data.Nat renaming (ℕ to Nat)
open import Data.List
open import Relation.Binary.Decidability
open import Relation.Binary.PropositionalEquality

infix 20 fvar_
infix 19 b_ f_

-- | represent free variables
record Fvar : Set where
  constructor fvar_
  field
    -- |there are countably infinite natural numbers,
    -- therefore it can represent countably infinite variables.
    sig : Nat

Fvars : Set
Fvars = List Fvar

instance
  Fvar-dec : Decidable {A = Fvar} _≡_
  Fvar-dec = record { dec = _v≟_ }
    where
      _v≟_ : ∀ (x y : Fvar) → Dec (x ≡ y)
      (fvar x) v≟ (fvar y) with x ≟ y
      (fvar x) v≟ (fvar .x) | yes refl = yes refl
      (fvar x) v≟ (fvar y)  | no ¬p    = no λ { refl → ¬p refl }

-- | A definition of variable, which can be either a De Bruijn index
-- or a named free variable.
data Avar : Set where
  b_ : Nat → Avar
  f_ : Fvar → Avar
