
open import Relation.Binary

module Data.List.HomoSet {c ℓ} (S : Setoid c ℓ) where

open import Level using (_⊔_)
open import Data.List
open import Data.List.Membership.Setoid S
open import Data.List.Membership.Setoid.Properties

open import Function

private
  cap = c ⊔ ℓ

open Setoid S renaming (Carrier to A)

data uniq : List A → Set cap where
  []  : uniq []
  _∷_ : ∀ {x xs} → x ∉ xs → uniq xs → uniq (x ∷ xs)


uniq-dropˡ : ∀ {l₁ l₂} → uniq (l₁ ++ l₂) → uniq l₁
uniq-dropˡ {[]} u++              = []
uniq-dropˡ {x ∷ l₁} (x∉++ ∷ u++) = (x∉++ ∘ ∈-++⁺ˡ S) ∷ uniq-dropˡ u++

uniq-dropʳ : ∀ l₁ {l₂} → uniq (l₁ ++ l₂) → uniq l₂
uniq-dropʳ [] u++ = u++
uniq-dropʳ (x ∷ l₁) (x∉++ ∷ u++) = uniq-dropʳ l₁ u++
