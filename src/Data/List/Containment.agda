{-# OPTIONS --safe #-}

module Data.List.Containment where

open import Data.List.Containment.Core public
open import Data.List as List public
import Data.List.NonEmpty as NeList
