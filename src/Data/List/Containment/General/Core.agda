{-# OPTIONS --safe #-}

module Data.List.Containment.General.Core where

open import Relation.Binary using (Setoid ; DecSetoid ; _Respects_)

import Data.List.Membership.Setoid as MSetoid
open import Data.List.Any using (Any; map; here; there)
open import Data.List public
open import Relation.Binary.Decidability
open import Function
open import Data.Product
open import Relation.Nullary using (¬_)
open import Data.List.Any.Membership.Properties using (module SingleSetoid)

module Containment {c ℓ} (S : Setoid c ℓ) where
  open Setoid S

  module ∈-mod = Membership S
  open ∈-mod public

  module singleSetoid = SingleSetoid S
  open singleSetoid public

  infix 4 _≊_  _!≊_  _⊂_  _⊄_
  _≊_ : List Carrier → List Carrier → Set _
  l₁ ≊ l₂ = l₁ ⊆ l₂ × l₂ ⊆ l₁

  _!≊_ : List Carrier → List Carrier → Set _
  l₁ !≊ l₂ = ¬ (l₁ ≊ l₂)

  -- |strictly set order
  _⊂_ : List Carrier → List Carrier → Set _
  l₁ ⊂ l₂ = l₁ ⊆ l₂ × l₂ ⊈ l₁
  
  _⊄_ : List Carrier → List Carrier → Set _
  l₁ ⊄ l₂ = ¬ (l₁ ⊂ l₂)



  ∈-resp-≈′ : ∀ {l} → (_∈ l) Respects _≈_  -- ∀ {a b : Carrier} {l} → a ≈ b → b ∈ l → a ∈ l
  ∈-resp-≈′ eq (here px)   = here (trans (sym eq) px)
  ∈-resp-≈′ eq (there b∈l) = there (∈-resp-≈′ eq b∈l)

  ∈∷ : ∀ {x y ys} → x ∈ ys → x ∈ y ∷ ys
  ∈∷ (here px)    = there (here px)
  ∈∷ (there x∈ys) = there (∈∷ x∈ys)

  drop-∷-⊆ : ∀ {x xs ys} → x ∷ xs ⊆ ys → xs ⊆ ys
  drop-∷-⊆ xxs⊆ys {a} a∈xs = xxs⊆ys (∈∷ a∈xs)


  
-- module DecContainment {c ℓ} (S : DecSetoid c ℓ) where
--   open DecSetoid S public

--   module containment = Containment setoid
--   open containment public

--   instance
--     ≈-dec : Decidable {A = Carrier} _≈_
--     ≈-dec = decidable _≟_

--   _≈?_ : (x y : Carrier) → Dec (x ≈ y)
--   _≈?_ = decide

--   instance
--     ∈-dec : Decidable {A = Carrier} _∈_
--     ∈-dec = record { dec = dec-helper }
--       where dec-helper : (e : Carrier) → (l : List Carrier) → Dec (e ∈ l)
--             dec-helper e []                       = no λ ()
--             dec-helper e (x ∷ l) with e ≈? x
--             dec-helper e (x ∷ l) | yes p          = yes $ here p
--             dec-helper e (x ∷ l) | no ¬p with dec-helper e l
--             dec-helper e (x ∷ l) | no ¬p | yes p  = yes $ there p
--             dec-helper e (x ∷ l) | no ¬p | no ¬p₁ = no λ { (here px) → ¬p px
--                                                          ; (there x₁) → ¬p₁ x₁
--                                                          }

--   infix 4 _∈?_
--   _∈?_ : (e : Carrier) → (l : List Carrier) → Dec (e ∈ l)
--   _∈?_ = decide

--   instance
--     ⊆-dec : Decidable {A = List Carrier} _⊆_
--     ⊆-dec = record { dec = ⊆-dec′}
--       where ⊆-dec′ : ∀ (x y : List Carrier) → Dec (x ⊆ y)
--             ⊆-dec′ [] y                          = yes λ ()
--             ⊆-dec′ (x ∷ l) y with x ∈? y
--             ⊆-dec′ (x ∷ l) y | yes x∈y with ⊆-dec′ l y
--             ⊆-dec′ (x ∷ l) y | yes x∈y | yes l⊆y = yes λ { (here px) → ∈-resp-≈ px x∈y
--                                                          ; (there x) → l⊆y x
--                                                          }
--             ⊆-dec′ (x ∷ l) y | yes x∈y | no l⊈y  = no λ xl⊆y → l⊈y (drop-∷-⊆ xl⊆y)
--             ⊆-dec′ (x ∷ l) y | no ¬p             = no $ λ f → ¬p (f (here refl))

--   _⊆?_ : (x y : List Carrier) → Dec (x ⊆ y)
--   _⊆?_ = decide

--   instance
--     ≊-dec : Decidable {A = List Carrier} _≊_
--     ≊-dec = record
--             { dec = ≊-dec′
--             } where
--       ≊-dec′ : (x y : List Carrier) → Dec (x ≊ y)
--       ≊-dec′ x y with x ⊆? y | y ⊆? x
--       ≊-dec′ x y | yes x⊆y | yes y⊆x = yes (x⊆y , y⊆x)
--       ≊-dec′ x y | yes x⊆y | no y⊈x  = no (λ z → y⊈x (proj₂ z))
--       ≊-dec′ x y | no x⊈y  | yes y⊆x = no (λ z → x⊈y (proj₁ z))
--       ≊-dec′ x y | no x⊈y  | no y⊈x  = no (λ z → y⊈x (proj₂ z))

--   _≊?_ : (x y : List Carrier) → Dec (x ≊ y)
--   _≊?_ = decide

--   instance
--     ⊂-dec : Decidable {A = List Carrier} _⊂_
--     ⊂-dec = record
--             { dec = ⊂-dec′
--             } where
--       ⊂-dec′ : (x y : List Carrier) → Dec (x ⊂ y)
--       ⊂-dec′ x y with x ⊆? y | y ⊆? x
--       ⊂-dec′ x y | yes x⊆y | yes y⊆x = no (λ z → proj₂ z y⊆x)
--       ⊂-dec′ x y | yes x⊆y | no y⊈x  = yes (x⊆y , y⊈x)
--       ⊂-dec′ x y | no x⊈y  | yes y⊆x = no (λ z → proj₂ z y⊆x)
--       ⊂-dec′ x y | no x⊈y  | no y⊈x  = no (λ z → x⊈y (proj₁ z))

--   _⊂?_ : (x y : List Carrier) → Dec (x ⊂ y)
--   _⊂?_ = decide
