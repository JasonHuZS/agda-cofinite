open import Relation.Binary.PropositionalEquality as ≡
  using (_≡_ ; refl ; setoid ; _≢_)
  renaming (trans to ≡-trans)
open import Relation.Binary

module Data.List.DecHomoSet {a} {A : Set a} (_≟_ : Decidable (_≡_ {A = A})) where

open import Data.Nat using (ℕ ; zero ; suc ; _+_)
open import Data.List
open import Data.List.Membership.DecPropositional _≟_
open import Relation.Nullary

open import Function

open import Data.Empty
open import Data.Product using (Σ ; _,_ ; proj₁ ; proj₂ ; _×_ ; ∃ ; ∃₂)

open import Data.Nat.Properties
open import Data.List.Properties

open import Data.List.HomoSet (setoid A) public

cmap : Set _
cmap = A → ℕ

empty : cmap
empty _ = 0

singleton : A → cmap
singleton a x with a ≟ x
... | yes _ = 1
... | no _  = 0

union : cmap → cmap → cmap
union m₁ m₂ x = m₁ x + m₂ x

collect : List A → cmap
collect []      = empty
collect (x ∷ l) = union (singleton x) (collect l)

union-comm : ∀ m₁ m₂ x → union m₁ m₂ x ≡ union m₂ m₁ x
union-comm m₁ m₂ x = +-comm (m₁ x) (m₂ x)

union-assoc : ∀ m₁ m₂ m₃ x → union (union m₁ m₂) m₃ x ≡ union m₁ (union m₂ m₃) x
union-assoc m₁ m₂ m₃ x = +-assoc (m₁ x) (m₂ x) (m₃ x)

collect-++-comm : ∀ l₁ l₂ x → collect (l₁ ++ l₂) x ≡ union (collect l₁) (collect l₂) x
collect-++-comm [] l₂ x           = refl
collect-++-comm (x₁ ∷ l₁) l₂ x
  rewrite collect-++-comm l₁ l₂ x = ≡.sym $ +-assoc (singleton x₁ x) (collect l₁ x) (collect l₂ x)

collect-swap : ∀ l₁ l₂ x → collect (l₁ ++ l₂) x ≡ collect (l₂ ++ l₁) x
collect-swap l₁ l₂ x
  rewrite collect-++-comm l₁ l₂ x
        | collect-++-comm l₂ l₁ x = +-comm (collect l₁ x) (collect l₂ x)

collect-[]-inv : ∀ l → (∀ x → collect l x ≡ 0) → l ≡ []
collect-[]-inv [] ev      = refl
collect-[]-inv (x ∷ l) ev with ev x
... | res with x ≟ x
collect-[]-inv (x ∷ l) ev | ()  | yes p
collect-[]-inv (x ∷ l) ev | res | no ¬p with ¬p refl
... | ()

collect-∷-inv : ∀ l x n → collect l x ≡ suc n →
  ∃₂ ( λ l₁ l₂ → l ≡ l₁ ++ x ∷ l₂
     × collect (l₁ ++ l₂) x ≡ n
     × ∀ x′ → x ≢ x′ → collect (l₁ ++ l₂) x′ ≡ collect l x′)
collect-∷-inv []       x n ()
collect-∷-inv (x₁ ∷ l) x n ev with x₁ ≟ x
... | yes refl with ev
... | refl = [] , l , refl , refl , helper
  where helper : ∀ x′ → x₁ ≢ x′ →
                   collect l x′ ≡ singleton x₁ x′ + collect l x′
        helper x′ x₁≢x′ with x₁ ≟ x′
        ... | yes refl = ⊥-elim $ x₁≢x′ refl
        ... | no _     = refl
collect-∷-inv (x₁ ∷ l) x n ev | no ¬p with collect-∷-inv l x n ev
... | l₁ , l₂ , refl , refl , h = x₁ ∷ l₁ , l₂ , refl , eqpf , helper
  where eqpf : singleton x₁ x + collect (l₁ ++ l₂) x ≡ collect (l₁ ++ l₂) x
        eqpf with x₁ ≟ x
        ... | yes refl              = ⊥-elim $ ¬p refl
        ... | no _                  = refl
        helper₁ : ∀ l₁ x′ → x ≢ x′ →
                    collect (l₁ ++ l₂) x′ ≡ collect (l₁ ++ x ∷ l₂) x′
        helper₁ [] x′ x≢x′ with x ≟ x′
        ... | yes refl              = ⊥-elim $ x≢x′ refl
        ... | no _                  = refl
        helper₁ (x ∷ l₁) x′ x≢x′
          rewrite helper₁ l₁ _ x≢x′ = refl
        helper : ∀ (x′ : A) → x ≢ x′ →
                   singleton x₁ x′ + collect (l₁ ++ l₂) x′ ≡ singleton x₁ x′ + collect (l₁ ++ x ∷ l₂) x′
        helper _ x≢x′
          rewrite helper₁ l₁ _ x≢x′ = refl

-- definition of permutation

-- an extentional definition based on multiset/bag
mperm : Rel (List A) _
mperm l₁ l₂ = ∀ x → collect l₁ x ≡ collect l₂ x

mperm-refl : Reflexive mperm
mperm-refl x = refl

mperm-sym : Symmetric mperm
mperm-sym m y = ≡.sym (m y)

mperm-trans : Transitive mperm
mperm-trans m₁₂ m₂₃ x = ≡-trans (m₁₂ x) (m₂₃ x)

-- an inductive definition
data perm : Rel (List A) a where
  unit  : ∀ {l} → perm l l
  prep  : ∀ {l₁ l₂} x → perm l₁ l₂ → perm (x ∷ l₁) (x ∷ l₂)
  swap  : ∀ {l₁ l₂} x y → perm l₁ l₂ → perm (x ∷ y ∷ l₁) (y ∷ x ∷ l₂)
  trans : ∀ {l₁ l₂ l₃} → perm l₁ l₂ → perm l₂ l₃ → perm l₁ l₃

perm-sym : ∀ {l₁ l₂} → perm l₁ l₂ → perm l₂ l₁
perm-sym unit         = unit
perm-sym (prep x p)   = prep x (perm-sym p)
perm-sym (swap x y p) = swap y x (perm-sym p)
perm-sym (trans p p₁) = trans (perm-sym p₁) (perm-sym p)

perm-++ʳ : ∀ {l₁ l₂} l → perm l₁ l₂ → perm (l₁ ++ l) (l₂ ++ l)
perm-++ʳ l unit         = unit
perm-++ʳ l (prep x p)   = prep x (perm-++ʳ l p)
perm-++ʳ l (swap x y p) = swap x y (perm-++ʳ l p)
perm-++ʳ l (trans p p₁) = trans (perm-++ʳ l p) (perm-++ʳ l p₁)

perm-++ˡ : ∀ {l₁ l₂} l → perm l₁ l₂ → perm (l ++ l₁) (l ++ l₂)
perm-++ˡ [] p      = p
perm-++ˡ (x ∷ l) p = prep x (perm-++ˡ l p)

perm-++ : ∀ {l₁ l₂ j₁ j₂} → perm l₁ l₂ → perm j₁ j₂ → perm (l₁ ++ j₁) (l₂ ++ j₂)
perm-++ {l₁} pl pj = trans (perm-++ˡ l₁ pj) (perm-++ʳ _ pl)

perm-inject : ∀ {l₁ l₂ j₁ j₂} x → perm l₁ l₂ → perm j₁ j₂ → perm (l₁ ++ x ∷ j₁) (l₂ ++ x ∷ j₂)
perm-inject {l₁} x pl pj = trans (perm-++ˡ l₁ (prep x pj)) (perm-++ʳ _ pl)

perm-swap-head : ∀ x l → perm (x ∷ l) (l ++ [ x ])
perm-swap-head x []       = unit
perm-swap-head x (x₁ ∷ l) = trans (swap x x₁ unit) (prep x₁ (perm-swap-head x l))

perm-swap : ∀ l₁ l₂ → perm (l₁ ++ l₂) (l₂ ++ l₁)
perm-swap [] l₂ rewrite ++-identityʳ l₂ = unit
perm-swap (x ∷ l₁) l₂ with perm-++ʳ l₁ $ perm-swap-head x l₂
... | res rewrite ++-assoc l₂ [ x ] l₁  = trans (prep x (perm-swap l₁ l₂)) res

perm-shift : ∀ l₁ l₂ x → perm (l₁ ++ x ∷ l₂) (x ∷ l₁ ++ l₂)
perm-shift [] l₂ x        = unit
perm-shift (x₁ ∷ l₁) l₂ x = trans (prep x₁ $ perm-shift l₁ l₂ x) (swap x₁ x unit)

-- a proof showing they are the same

perm⇒mperm : ∀ {l₁ l₂} → perm l₁ l₂ → mperm l₁ l₂
perm⇒mperm unit x                      = refl
perm⇒mperm (prep x₁ p) x
  rewrite perm⇒mperm p x               = refl
perm⇒mperm (swap {l₂ = l₂} a b p) x
  rewrite perm⇒mperm p x with singleton a x | singleton b x
... | sa | sb
  rewrite ≡.sym $ +-assoc sa sb (collect l₂ x)
        | +-comm sa sb
        | +-assoc sb sa (collect l₂ x) = refl
perm⇒mperm (trans p p₁) x
  rewrite perm⇒mperm p x
        | perm⇒mperm p₁ x              = refl

mperm⇒perm : ∀ {l₁ l₂} → mperm l₁ l₂ → perm l₁ l₂
mperm⇒perm {[]}  {l₂} ev
  rewrite collect-[]-inv l₂ (≡.sym ∘ ev) = unit
mperm⇒perm {x ∷ l₁} {l₂} ev with ev x
... | res with x ≟ x
... | yes refl with collect-∷-inv l₂ x _ (≡.sym res)
... | lh , lt , refl , q , h             = trans (prep x $ mperm⇒perm {l₁} helper)
                                                 (perm-sym $ perm-shift lh lt x)
  where helper : mperm l₁ (lh ++ lt)
        helper y with ev y
        ... | res with x ≟ y
        ... | yes refl = ≡.sym q
        ... | no x≢y   = ≡-trans res (≡.sym $ h _ x≢y)
mperm⇒perm {x ∷ l₁} {l₂} ev | _ | no ¬p = ⊥-elim $ ¬p refl


mperm-isEquivalence : IsEquivalence mperm
mperm-isEquivalence = record
  { refl  = λ {l} → mperm-refl {l}
  ; sym   = λ {l} {l′} → mperm-sym {l} {l′}
  ; trans = λ {l₁} {l₂} {l₃} → mperm-trans {l₁} {l₂} {l₃}
  }

mperm-setoid : Setoid _ _
mperm-setoid = record { isEquivalence = mperm-isEquivalence }

perm-isEquivalence : IsEquivalence perm
perm-isEquivalence = record
  { refl  = unit
  ; sym   = perm-sym
  ; trans = trans
  }

perm-setoid : Setoid _ _
perm-setoid = record { isEquivalence = perm-isEquivalence }

open import Data.List.Any

perm-resp-∈ : ∀ x → (x ∈_) Respects perm
perm-resp-∈ x unit x∈xs                          = x∈xs
perm-resp-∈ .x₁ (prep x₁ p) (here refl)          = here refl
perm-resp-∈ x (prep x₁ p) (there x∈xs)           = there (perm-resp-∈ x p x∈xs)
perm-resp-∈ .x₁ (swap x₁ y p) (here refl)        = there (here refl)
perm-resp-∈ .y (swap x₁ y p) (there (here refl)) = here refl
perm-resp-∈ x (swap x₁ y p) (there (there x∈xs)) = there (there (perm-resp-∈ x p x∈xs))
perm-resp-∈ x (trans p p₁) x∈xs                  = perm-resp-∈ x p₁ (perm-resp-∈ x p x∈xs) 
