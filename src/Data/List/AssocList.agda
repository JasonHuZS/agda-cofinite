open import Relation.Binary
open import Relation.Binary.PropositionalEquality

module Data.List.AssocList {a} {K : Set a} (_≟_ : Decidable (_≡_ {A = K})) where

open import Relation.Nullary using (¬_ ; Dec ; yes ; no)

open import Level
open import Data.List
open import Data.Sum
open import Data.Product
open import Data.List.Any

open import Data.Empty

open import Data.List.DecHomoSet _≟_

open import Function

module ValSpace {ℓ} {V : K → Set a}
  {decV : (k : K) → V k → V k → Set ℓ}
  (decV-equiv : (k : K) → IsEquivalence (decV k))
  where

  decV-setoid : K → Setoid _ _
  decV-setoid k = record { isEquivalence = (decV-equiv k) }

  AssocList : Set _
  AssocList = List (Σ K V)

  dom : AssocList → List K
  dom []             = []
  dom ((k , _) ∷ al) = k ∷ dom al

  uniq-dom : AssocList → Set _
  uniq-dom al = uniq (dom al)

  _≈_ : Σ K V → Σ K V → Set _
  (k₁ , v₁) ≈ (k₂ , v₂) = Σ (k₁ ≡ k₂) λ { refl → decV k₁ v₁ v₂ }

  Σ-refl : Reflexive _≈_
  Σ-refl {(k , v)} = refl , IsEquivalence.refl (decV-equiv k)

  Σ-sym : Symmetric _≈_
  Σ-sym {(k₁ , v₁)} {(k₂ , v₂)} (refl , pf) = refl , IsEquivalence.sym (decV-equiv k₁) pf

  Σ-trans : Transitive _≈_
  Σ-trans {(k₁ , v₁)} {(k₂ , v₂)} {(k₃ , v₃)} (refl , pf₁) (refl , pf₂)
    = refl , (IsEquivalence.trans (decV-equiv k₁) pf₁ pf₂)

  Σ-isEquivalence : IsEquivalence _≈_
  Σ-isEquivalence = record
    { refl  = Σ-refl
    ; sym   = Σ-sym
    ; trans = Σ-trans
    }
  
  Σ-setoid : Setoid _ _
  Σ-setoid = record { isEquivalence = Σ-isEquivalence }

  open import Data.List.Membership.DecPropositional _≟_ using ()
    renaming (_∈_ to _∈ₖ_) public

  open import Data.List.Membership.Setoid Σ-setoid

  infix 5 _[_↦_] _⟨_↦_⟩

  -- pronounced "binds"
  _[_↦_] : AssocList → (x : K) → V x → Set _
  l [ x ↦ v ] = (x , v) ∈ l

  -- importing some properties for binds
  open import Data.List.Membership.Setoid.Properties

  binds-++ˡ : ∀ {l₁ l₂ k v} → l₁ [ k ↦ v ] → (l₁ ++ l₂) [ k ↦ v ]
  binds-++ˡ = ∈-++⁺ˡ Σ-setoid

  binds-++ʳ : ∀ l₁ {l₂ k v} → l₂ [ k ↦ v ] → (l₁ ++ l₂) [ k ↦ v ]
  binds-++ʳ l₁ = ∈-++⁺ʳ Σ-setoid l₁

  binds-++ : ∀ l₁ {l₂ k v} → (l₁ ++ l₂) [ k ↦ v ] → l₁ [ k ↦ v ] ⊎ l₂ [ k ↦ v ]
  binds-++ l₁ = ∈-++⁻ Σ-setoid l₁
  
  infix 4 _∈d_ _∉d_

  _∈d_ : K → AssocList → Set _
  k ∈d al = k ∈ₖ dom al

  _∉d_ : K → AssocList → Set _
  k ∉d al = ¬ k ∈d al

  ∈d⇒binds : ∀ {k al} → k ∈d al → ∃ (λ v → al [ k ↦ v ])
  ∈d⇒binds {k} {[]} ()
  ∈d⇒binds {k} {(x , v) ∷ al} (here refl) = v , here Σ-refl
  ∈d⇒binds {k} {(x , v) ∷ al} (there k∈al) with ∈d⇒binds k∈al
  ... | v′ , al[k↦v′]                 = v′ , there al[k↦v′]

  binds⇒∈d : ∀ {al k v} → al [ k ↦ v ] → k ∈d al
  binds⇒∈d (here (refl , _)) = here refl
  binds⇒∈d (there al[k↦v]) = there (binds⇒∈d al[k↦v])

  ∉d⇒¬binds : ∀ {al k v} → k ∉d al → al [ k ↦ v ] → ⊥
  ∉d⇒¬binds k∉dal pf = k∉dal (binds⇒∈d pf)

  uniq-binds : ∀ {k al v₁ v₂} → uniq-dom al →
                al [ k ↦ v₁ ] → al [ k ↦ v₂ ] → decV k v₁ v₂
  uniq-binds {k} {[]} ual () ()
  uniq-binds {.k′} {(k′ , v′) ∷ al} ual (here (refl , eq₁)) (here (refl , eq₂))      = vtrans eq₁ (vsym eq₂)
    where open Setoid (decV-setoid k′) renaming (trans to vtrans ; sym to vsym)
  uniq-binds {.k′} {(k′ , v′) ∷ al} (k∉dal ∷ ual) (here (refl , _)) (there al[k↦v₂]) = ⊥-elim ∘ k∉dal $ binds⇒∈d al[k↦v₂]
  uniq-binds {.k′} {(k′ , v′) ∷ al} (k∉dal ∷ ual) (there al[k↦v₁]) (here (refl , _)) = ⊥-elim ∘ k∉dal $ binds⇒∈d al[k↦v₁]
  uniq-binds {k}   {(k′ , v′) ∷ al} (k∉dal ∷ ual) (there al[k↦v₁]) (there al[k↦v₂])  = uniq-binds ual al[k↦v₁] al[k↦v₂]

  Dict : Set _
  Dict = Σ AssocList uniq-dom

  _⟨_↦_⟩ : Dict → (x : K) → V x → Set _
  _⟨_↦_⟩ = _[_↦_] ∘ proj₁


module DecValSpace {V : K → Set a}
  (decV : (k : K) → Decidable (_≡_ {A = V k}))
  where

  decV-equiv : ∀ k → IsEquivalence (_≡_ {A = V k})
  decV-equiv k = Setoid.isEquivalence (setoid (V k))

  open ValSpace decV-equiv public

  _≈?_ : ∀ t₁ t₂ → Dec (t₁ ≈ t₂)
  (k₁ , v₁) ≈? (k₂ , v₂) with k₁ ≟ k₂
  ... | yes refl with decV k₁ v₁ v₂
  ... | yes refl                  = yes (refl , refl)
  ... | no v₁≠v₂                  = no λ { (refl , refl) → v₁≠v₂ refl }
  (k₁ , _) ≈? (k₂ , _) | no k₁≠k₂ = no λ { (refl , refl) → k₁≠k₂ refl }

  Σ-isDecEquivalence : IsDecEquivalence _≈_
  Σ-isDecEquivalence = record { isEquivalence = Σ-isEquivalence ; _≟_ = _≈?_ }

  Σ-decSetoid : DecSetoid _ _
  Σ-decSetoid = record { isDecEquivalence = Σ-isDecEquivalence }


-- module _ {V₁ : K → Set a} {V₂ : K → Set a} where


